package com.example.cuento
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_registro.*
class RegistroActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        btnRegistrarse.setOnClickListener(){
            val email = edtEmail.text.toString()
            val nombre = edtNombre.text.toString()
            val apellido = edtApellido.text.toString()
            val password = edtContrasenia.text.toString()
            val confirm_pass = edtRepetirContrasenia.text.toString()

            if(nombre.isEmpty()){
                Toast.makeText(this,"Obligatorio ingresar un nombre", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(email.isEmpty()){
                Toast.makeText(this,"Obligatorio ingresar un correo electronico", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
           if(apellido.isEmpty()){
                Toast.makeText(this,"Obligatorio ingresar un apellido", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(password.isEmpty()){
                Toast.makeText(this,"Obligatorio ingresar una contraseña", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(confirm_pass.isEmpty()){
                Toast.makeText(this,"Obligatorio ingresar la confirmacion de la contraseña", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val bundle = Bundle()
            bundle.apply {
                putString("dato1",nombre)
            }
            val intent = Intent(this,SeleccionarActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}