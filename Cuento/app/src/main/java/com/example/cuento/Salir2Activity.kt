package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_parte1.*
import kotlinx.android.synthetic.main.activity_salir2.*
import kotlinx.android.synthetic.main.activity_seleccionar.*
import kotlinx.android.synthetic.main.activity_seleccionar.tvNombre

class Salir2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salir2)

        val bundle : Bundle? = intent.extras
        bundle?.let {
            val name = it.getString("dato1")
            tvNombre.text = "$name"
        }
        btnSalir2.setOnClickListener(){
        //finish()
        //System.exit(0)
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }

    }
}