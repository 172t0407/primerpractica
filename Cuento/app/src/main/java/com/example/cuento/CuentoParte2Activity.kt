package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_parte1.*
import kotlinx.android.synthetic.main.activity_cuento_parte2.*
import kotlinx.android.synthetic.main.activity_salir2.*
import kotlinx.android.synthetic.main.activity_cuento_parte2.tvNombre as tvNombre1
import kotlinx.android.synthetic.main.activity_salir2.tvNombre as tvNombre1

class CuentoParte2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento_parte2)

    val bundle : Bundle? = intent.extras
    bundle?.let {
        val name = it.getString("dato1")
        tvNombre.text = "$name"
    }
    btnSiguiente2.setOnClickListener(){
        val nombre = tvNombre.text.toString()
        val bundle = Bundle()
        bundle.apply {
            putString("dato1", nombre)
        }
        val intent = Intent(this,CuentoParte3::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
        }
    }
}