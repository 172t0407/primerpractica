package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_parte1.*
import kotlinx.android.synthetic.main.activity_cuento_parte3.*
import kotlinx.android.synthetic.main.activity_cuento_parte4.*
import kotlinx.android.synthetic.main.activity_salir2.*
import kotlinx.android.synthetic.main.activity_cuento_parte1.tvNombre as tvNombre1
import kotlinx.android.synthetic.main.activity_cuento_parte3.tvNombre as tvNombre1
import kotlinx.android.synthetic.main.activity_salir2.tvNombre as tvNombre1

class CuentoParte4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento_parte4)

      val bundle : Bundle? = intent.extras
    bundle?.let {
        val name = it.getString("dato1")
        tvNombre.text = "$name"
    }
    btnSiguiente4.setOnClickListener(){
        val nombre = tvNombre.text.toString()
        val bundle = Bundle()
        bundle.apply {
            putString("dato1", nombre)
        }
        val intent = Intent(this,DecisionActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
        }
    }
}