package com.example.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registro.*
import kotlinx.android.synthetic.main.activity_seleccionar.*

class SeleccionarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seleccionar)
        val bundle : Bundle? = intent.extras
        bundle?.let {
            val name = it.getString("dato1")
            tvNombre.text = "$name"
        }
        btnVamos.setOnClickListener(){
            val nombre = tvNombre.text.toString()
            val bundle = Bundle()
            bundle.apply {
                putString("dato1", nombre)
            }
            val intent = Intent(this,CuentoParte1Activity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}